import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ProductsPage } from '../products/products';
import { RecipesPage } from '../recipes/recipes';
import { ShoppingPage } from '../shopping/shopping';
import { SettingsPage } from '../settings/settings';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1: any;
  tab2: any;
  tab3: any;
  tab4: any;

  constructor(public navParams: NavParams) {
    this.tab1 = RecipesPage;
    this.tab2 = ProductsPage;
    this.tab3 = ShoppingPage;
    this.tab4 = SettingsPage;
  }
}
