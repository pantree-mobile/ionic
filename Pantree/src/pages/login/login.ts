import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { IUser } from '../../models/IUser';
import { UsersProvider } from '../../providers/users/users';
import { TabsPage } from '../tabs/tabs';
import { IAuth } from '../../models/IAuth';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user : IUser = { email : "test@test.com", password: "azerty"}
  constructor(private usersProviders : UsersProvider,private navCtrl: NavController,private toast: Toast) {
  }

  ionViewDidLoad() {
    if(localStorage.getItem('token') != undefined){
      this.navCtrl.setRoot(TabsPage).catch(err => console.log(err));
    }
  }

  signIn(){
    this.usersProviders.login(this.user).subscribe((auth) => {
      if(auth.access_token != null){
        localStorage.setItem('token', auth.access_token);
        localStorage.setItem('token_expires_at', auth.expires_at);
        this.navCtrl.setRoot(TabsPage);
      }
    },(error) =>{
      console.log(error);
    });
  }

}
