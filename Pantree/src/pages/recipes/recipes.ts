import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecipesProvider } from '../../providers/recipes/recipes';
import { IRecipe } from '../../models/IRecipe';

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {
  recipes: IRecipe[] = [];

  constructor(private navCtrl: NavController, private navParams: NavParams, private recipesProvider: RecipesProvider) {
  }

  ionViewWillEnter() {
    this.recipesProvider.getRecipesSuggestions().subscribe(data => this.recipes = data);
  }

  doRefresh(refresher) {
    this.recipesProvider.getRecipesSuggestions().subscribe(data => {
      this.recipes = data;
      refresher.complete();
    }, err => refresher.complete());
  }

  viewUrl(url) {
    window.open(url, '_blank');
  }
}
