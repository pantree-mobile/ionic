import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingDetailsPage } from './shopping-details';

@NgModule({
  declarations: [
    ShoppingDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppingDetailsPage),
  ],
})
export class ShoppingDetailsPageModule {}
