import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {ShoppingListProvider} from "../../providers/shopping-list/shopping-list";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AddProductPage } from '../add-product/add-product';
import { IProduct } from '../../models/IProduct';
import {IShoppingList} from "../../models/IShoppingList";

@IonicPage()
@Component({
  selector: 'page-shopping-details',
  templateUrl: 'shopping-details.html',
})
export class ShoppingDetailsPage {

  shoppingList: IShoppingList;
  shoppingListId: number;
  count = {count:1};

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl: ModalController, private shoppingListsProvider : ShoppingListProvider, private barcodeScanner: BarcodeScanner) {
    this.shoppingListId = navParams.data.id;
  }

  ionViewWillEnter() {
    this.shoppingListsProvider.getShoppingList(this.shoppingListId).subscribe((shoppingList) => {
      this.shoppingList = shoppingList
    });
  }

  doRefresh(refresher) {
    this.shoppingListsProvider.getShoppingList(this.shoppingListId).subscribe(shoppingList => {
      this.shoppingList = shoppingList;
      refresher.complete();
    });
  }

  scan() {
    this.barcodeScanner.scan().then((barcodeData) => {
      this.count.count = 1;
      var product : IProduct = { code : barcodeData.text};  
      this.addProduct(product,this.count.count);
    }, (err) => {
      console.log(err)
    }).catch(err => console.log(err));
  }

  presentModal() {
    const modal = this.modalCtrl.create(AddProductPage,this.shoppingList);
    modal.onDidDismiss((product) => {
      
      if(!product || product.length == 0){
        return;
      }
      this.shoppingList['product'].push(product.id);
    });

    modal.present().catch(err => console.log(err));
  }

  delete(shoppingList) {
    this.shoppingListsProvider.delete(shoppingList.id).subscribe((data) => {
      this.navCtrl.pop().catch(err => console.log(err));
    });
  }

  productBought(product){
    this.shoppingListsProvider.productBought(product, this.shoppingListId).subscribe((returnedProduct) => {
      //this.deleteProduct(product)
      //this.shoppingList['products'].push(product);
    });
    this.deleteProduct(product)
  }

  addOne(product){
    //todo have a count for shopping list
    product.shoppinglistproduct.count ++;
    this.count.count = product.shoppinglistproduct.count;
    this.addProduct(product,this.count.count);
  }

  keepOne(product){
    //todo have a count for shopping list
    product.shoppinglistproduct.count = 1;
    this.count.count = product.shoppinglistproduct.count;
    this.addProduct(product,this.count.count);
  }

  deleteProduct(product){
    //todo have a count for shopping list
    product.shoppinglistproduct.count = "0";
    this.count.count = product.shoppinglistproduct.count;
    this.addProduct(product,this.count.count);
    this.shoppingList['products'].splice(this.shoppingList['products'].indexOf(product), 1);
  }
  
  addProduct(product,count){
    this.shoppingListsProvider.addToShoppingList(product, this.shoppingListId, count).subscribe((product) => {
      this.shoppingList.products.push(product);
    }, err => {
      this.shoppingList.products.push(product);
    });
  }
}
