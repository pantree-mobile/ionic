import { Component } from '@angular/core';
import {AlertController, IonicPage} from 'ionic-angular';
import { ProductsProvider } from '../../providers/products/products';
import { IProduct } from '../../models/IProduct';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {ShoppingListProvider} from "../../providers/shopping-list/shopping-list";

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  products: IProduct[] = [];
  count = {count:1};
  productFound:boolean = false;
  constructor(private productsProviders : ProductsProvider, private shoppingListsProvider: ShoppingListProvider, private barcodeScanner: BarcodeScanner, private alertCtrl: AlertController) {
  }

  ionViewWillEnter() {
    this.productsProviders.getProducts().subscribe(data => this.products = data);
  }

  doRefresh(refresher) {
    this.productsProviders.getProducts().subscribe(data => {
      this.products = data;
      refresher.complete();
    }, err => refresher.complete());
  }

  scan() {
    this.barcodeScanner.scan().then((barcodeData) => {
      this.count.count = 1;
      this.addProduct(barcodeData.text,this.count.count);
    }, (err) => {
      console.log(err)
    }).catch(err => console.log(err));
  }

  addProduct(productId,count){
    let data = { productId,count};
    this.productsProviders.addProducts(data).subscribe((product) => {
      this.products.push(product);
    });
  }

  addOne(product){
    product.userproduct.count ++;
    this.count.count = product.userproduct.count;
    this.addProduct(product.code,this.count.count);
  }

  keepOne(product){
    product.userproduct.count = 1;
    this.count.count = product.userproduct.count;
    this.addProduct(product.code,this.count.count);
  }

  delete(product){
    product.userproduct.count = "0";
    this.count.count = product.userproduct.count;
    this.addProduct(product.code,this.count.count);
    this.products.splice(this.products.indexOf(product), 1);
  }

  addToShoppingList(product) {
    this.shoppingListsProvider.getShoppingLists().subscribe((shoppingLists) => {
      let alert = this.alertCtrl.create();
      alert.setTitle("Which shopping list?");

      shoppingLists.forEach((shoppingList) => {
        alert.addInput({ type: 'radio', label: shoppingList.name, value: shoppingList.id.toString() });
      });

      alert.addButton('Cancel');
      alert.addButton({
        text: 'Add product',
        handler: shoppingListId => {
          let count = 1;
          this.shoppingListsProvider.addToShoppingList(product, shoppingListId, count).subscribe((data) => {
            console.log(data);
          });
        }
      });
      alert.present().catch(err => console.log(err));
    });
  }
}
