import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, Platform} from 'ionic-angular';
import { ShoppingListProvider } from '../../providers/shopping-list/shopping-list';
import { IProduct } from '../../models/IProduct';
import { ProductsProvider } from '../../providers/products/products';

@IonicPage()
@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html',
})
export class AddProductPage {

  product : IProduct = { product_name : ""};  
  count: string = '';
  items: IProduct[];
  shoppingList = {};

  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController,private shoppingListProviders : ShoppingListProvider,private productProviders : ProductsProvider) {
    this.shoppingList = params.data;
  }

  dismiss() {
    this.viewCtrl.dismiss().catch(err => console.log(err));
  }

  add(){
    this.addProduct(this.product,this.count)
  }

  getItems(search) {
    this.productProviders.searchProducts(search).subscribe((products) => {
      console.log(products);
      // set val to the value of the searchbar
      this.items = products;
    });
    
  }

  touchItem(item){
    this.product = item;
    this.items= null;
  }

  addProduct(product,count){
    this.shoppingListProviders.addToShoppingList(product, this.shoppingList['id'], count).subscribe((product) => {
      this.shoppingList['products'].push(product);
      this.shoppingListProviders.addShoppingList(this.product).subscribe((product) => {
        this.viewCtrl.dismiss(product).catch(err => console.log(err));
      });
    });
    this.viewCtrl.dismiss().catch(err => console.log(err));
  }
}
