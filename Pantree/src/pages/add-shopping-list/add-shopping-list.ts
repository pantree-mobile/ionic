import { Component } from '@angular/core';
import { IonicPage, NavParams, Platform, ViewController } from 'ionic-angular';
import { ShoppingListProvider } from '../../providers/shopping-list/shopping-list';
import { IShoppingList } from '../../models/IShoppingList';

@IonicPage()
@Component({
  selector: 'page-add-shopping-list',
  templateUrl: 'add-shopping-list.html',
})
export class AddShoppingListPage {

  shoppingList : IShoppingList = { name : "Shopping list name", date: "01/09/2018"};

  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController, private shoppingListProviders : ShoppingListProvider) {}

  dismiss() {
    this.viewCtrl.dismiss().catch(err => console.log(err));
  }

  add(){
    this.shoppingListProviders.addShoppingList(this.shoppingList).subscribe((shoppingList) => {
      this.viewCtrl.dismiss(shoppingList).catch(err => console.log(err));
    });
  }
}
