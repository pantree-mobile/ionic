import { Component } from '@angular/core';
import { App, IonicPage } from 'ionic-angular';
import { UsersProvider } from '../../providers/users/users';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(private app: App, private usersProviders : UsersProvider) {}

  disconnect(){
    this.usersProviders.disconnect().subscribe(() => {
      localStorage.clear();
      this.app.getRootNavs()[0].setRoot(LoginPage);
    });
  }
}
