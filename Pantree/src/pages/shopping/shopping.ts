import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { ShoppingDetailsPage } from '../shopping-details/shopping-details';
import { AddShoppingListPage } from '../add-shopping-list/add-shopping-list';
import { ShoppingListProvider } from '../../providers/shopping-list/shopping-list';
import { IShoppingList } from '../../models/IShoppingList';

@IonicPage()
@Component({
  selector: 'page-shopping',
  templateUrl: 'shopping.html',
})
export class ShoppingPage {
  shoppingLists: IShoppingList[] = [];

  constructor(private shoppingListProviders : ShoppingListProvider,private navCtrl: NavController,private modalCtrl: ModalController) {}

  ionViewWillEnter() {
    this.shoppingListProviders.getShoppingLists().subscribe(data => this.shoppingLists = data);
  }

  viewDetails(shoppingList) {
    this.navCtrl.push(ShoppingDetailsPage, shoppingList).catch(err => console.error(err));
  }

  presentModal() {
    const modal = this.modalCtrl.create(AddShoppingListPage);
    modal.onDidDismiss((shoppingList) => {
      if(!shoppingList || shoppingList.length == 0){
        return;
      }
      this.shoppingLists.push(shoppingList)
    });

    modal.present().catch(err => console.log(err));
  }

  doRefresh(refresher) {
    this.shoppingListProviders.getShoppingLists().subscribe(data => {
      this.shoppingLists = data;
      refresher.complete();
    });
  }
}
