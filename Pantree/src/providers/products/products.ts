import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IProduct } from '../../models/IProduct';

@Injectable()
export class ProductsProvider {

  private static BASE_URL : String = "https://pantree.ovh:3000";

  constructor(public http: HttpClient) { }

  getProducts() : Observable<IProduct[]> {
    return this.http.get<any>(ProductsProvider.BASE_URL + "/me/products", {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  searchProducts(search) : Observable<IProduct[]> {
    return this.http.get<any>(ProductsProvider.BASE_URL + "/products?query=" + search);
  }

  addProducts(data) : Observable<IProduct> {
    return this.http.post<any>(ProductsProvider.BASE_URL + "/me/products",data, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }
}
