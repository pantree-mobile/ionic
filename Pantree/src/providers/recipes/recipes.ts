import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IRecipe } from "../../models/IRecipe";

@Injectable()
export class RecipesProvider {

  private static BASE_URL : String = "https://pantree.ovh:3000";

  constructor(public http: HttpClient) { }

  getRecipes() : Observable<IRecipe[]> {
    return this.http.get<IRecipe[]>(RecipesProvider.BASE_URL + "/me/recipes", {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  getRecipesSuggestions() : Observable<IRecipe[]> {
    return this.http.get<IRecipe[]>(RecipesProvider.BASE_URL + "/me/recipes/suggestions", {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }
}
