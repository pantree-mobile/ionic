import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IShoppingList } from '../../models/IShoppingList';
import { IProduct } from '../../models/IProduct';

@Injectable()
export class ShoppingListProvider {

  private static BASE_URL : String = "https://pantree.ovh:3000";

  constructor(public http: HttpClient) { }

  getShoppingLists() : Observable<IShoppingList[]> {
    return this.http.get<any>(ShoppingListProvider.BASE_URL + "/me/shopping-lists", {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  getShoppingList(id) {
    return this.http.get<IShoppingList>(ShoppingListProvider.BASE_URL + "/me/shopping-lists/" + id, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  addShoppingList(shoppingList) : Observable<IShoppingList>{
    return this.http.post<any>(ShoppingListProvider.BASE_URL + "/me/shopping-lists",shoppingList, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  addToShoppingList(product, shoppingListId, count) {
    return this.http.post<any>(ShoppingListProvider.BASE_URL + "/me/shopping-lists/" + shoppingListId + "/products", { productId: product.code, count: count }, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  delete(id): Observable<any> {
    return this.http.delete(ShoppingListProvider.BASE_URL + "/me/shopping-lists/" + id, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }

  productBought(product,shoppingListId) : Observable<IProduct>{
    return this.http.post<any>(ShoppingListProvider.BASE_URL + "/me/shopping-lists/" + shoppingListId + "/products/" + product.code + "/buy", { product: product}, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }
}
