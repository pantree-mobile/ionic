import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IAuth } from '../../models/IAuth';

@Injectable()
export class UsersProvider {

  private static BASE_URL : String = "https://pantree.ovh:3000";

  constructor(public http: HttpClient) { }

  login(user) : Observable<IAuth>{
    return this.http.post<any>(UsersProvider.BASE_URL + "/auth/login",user);
  }

  disconnect() : Observable<IAuth>{
    return this.http.post<any>(UsersProvider.BASE_URL + "/auth/logout",null, {
      headers: new HttpHeaders().append('Authorization',localStorage.getItem('token'))
    });
  }
}
