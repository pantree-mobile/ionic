import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ShoppingPageModule } from '../pages/shopping/shopping.module';
import { RecipesPageModule } from '../pages/recipes/recipes.module';
import { ProductsPageModule } from '../pages/products/products.module';
import { SettingsPageModule } from '../pages/settings/settings.module';
import { TabsPageModule } from '../pages/tabs/tabs.module';
import { RecipesProvider } from '../providers/recipes/recipes';
import { UsersProvider } from '../providers/users/users';
import { ProductsProvider } from '../providers/products/products';
import { ShoppingListProvider } from '../providers/shopping-list/shopping-list';
import { LoginPageModule } from '../pages/login/login.module';
import { ShoppingDetailsPageModule } from '../pages/shopping-details/shopping-details.module';
import { AddShoppingListPageModule } from '../pages/add-shopping-list/add-shopping-list.module';
import { HttpClientModule } from '@angular/common/http';
import { AddProductPageModule } from '../pages/add-product/add-product.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    AddShoppingListPageModule,
    AddProductPageModule,
    LoginPageModule,
    ProductsPageModule,
    RecipesPageModule,
    ShoppingPageModule,
    ShoppingDetailsPageModule,
    SettingsPageModule,
    TabsPageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RecipesProvider,
    UsersProvider,
    ProductsProvider,
    ShoppingListProvider,
    BarcodeScanner,
    Toast
  ]
})
export class AppModule {}
