import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { UsersProvider } from '../providers/users/users';
import { RecipesPage } from '../pages/recipes/recipes';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = this.getRootPage();
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  getRootPage(){
      var page:any;
      var myDate: String = Math.round(new Date().getTime()/1000).toString();    
      if(myDate >= localStorage.getItem('token_expires_at') || localStorage.getItem('token_expires_at') == undefined){
        localStorage.clear();
        page = LoginPage;
      }else{
        page = TabsPage;
      }
      return page;
  }
}
