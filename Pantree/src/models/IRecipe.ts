export interface IRecipe {
    id?: number;
    title: string;
    href : string;
    ingredients : string;
    thumbnail : string;
}
