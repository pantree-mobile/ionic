export interface IAuth {
    access_token: string;
    expires_at: string;
}
