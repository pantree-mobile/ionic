export interface IProduct {
    code?: string;
    product_name?: string;
    categories?:string;
    images_url?:string;
    image_small_url?:string;
    product_quantity?:number;
    quantity?:string;
    keywords?:string[];
    userproduct?: {
        count?: number;
        userId?: number;
    };
}
