import {IProduct} from "./IProduct";

export interface IShoppingList {
    id?: number;
    name: string;
    date?: string;
    products?: IProduct[];
}
