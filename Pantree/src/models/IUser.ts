import { IProduct } from "./IProduct";
import { IRecipe } from "./IRecipe";
import { IShoppingList } from "./IShoppingList";

export interface IUser {
    id?: number;
    email: string;
    password?: string;
    products? : IProduct[];
    shoppinglists? : IShoppingList[];
    recipes? : IRecipe[];
}
